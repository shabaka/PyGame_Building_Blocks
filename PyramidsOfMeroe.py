# -*- coding: utf-8 -*-
"""
Created on Wed May 31 18:57:55 2017

@author: Shabaka
"""

import pygame


def checkCollision(x,y,treasureX,treasureY):
    global screen,textWin
    collisionState = False
        # define conditions that restrict player movement
    if y >= treasureY and y <= treasureY + 40:
        if x >= treasureX and x <= treasureX + 30:
            y = 540
            collisionState = True
            
        elif x + 35 >= treasureX and x + 35 <= treasureX +30:
            y = 540
            collisionState = True
            
    elif y + 40 >= treasureY and y + 40 <= treasureY +40:
        if x >= treasureX and x <= treasureX + 30:
            y = 540
            collisionState = True
            
        elif x + 35 >= treasureX and x + 35 <= treasureX + 30:
            y = 540
            collisionState = True
            
    return collisionState, y

pygame.init()
screen = pygame.display.set_mode((600, 550))

finished = False # check condition : e.g .0 < 10 - > True/ 10<10 -> False
x = 300 - 30/2 # ideally should be aligned to the centre of your background image/screen
y = 500

# we create a player image to overlay on our rectangle

playerImage = pygame.image.load("EV3 Brick Image.jpg")
playerImage = pygame.transform.scale(playerImage, (30, 40))
playerImage = playerImage.convert_alpha()
backgroundImage = pygame.image.load("Meroe_Pyramids_Sudan.jpg")
backgroundImage = pygame.transform.scale(backgroundImage, (600, 550))
screen.blit(backgroundImage, (0, 0))

# Define Treasure
treasureImage = pygame.image.load("QalhataTomb.jpg") # call the treasure image
treasureImage = pygame.transform.scale(treasureImage, (30, 40))
treasureImage = treasureImage.convert_alpha()

enemyImage = pygame.image.load("EV3 Brick Image.jpg") # add image here
enemyImage = pygame.transform.scale(enemyImage, (30, 40))
enemyImage = enemyImage.convert_alpha()
# Define the treasure x and y positions:

treasureX = 300 - 30/2
treasureY = 75


enemyX = 75
enemyY = 450 - 15


screen.blit(treasureImage, (treasureX, treasureY))

font = pygame.font.SysFont("comicsans", 40)
level = 1

# create text object that provides update on game status
#textWin = font.render("Great Job!", True, (0,0,0))
#textWin = font.render("Next Level Reached!", True, (0,0,0))

 # ''''''''''''''' Note on Dictionaries ''''''''''''''''''#
# General structure:
#dictionary = {Key:Value,
#              Key2:value2} # good for fast look ups , order is not important like in lists

enemyNames = {0: "Dooshi", 1:"Booshi", 2: "Cooshi", 3: "Nooshi"}

# We can limit the frame rate to optimise use of resource -
#  we do this by defining a frame object to use
# print pygame.K_SPACE

frame = pygame.time.Clock()

collisionTreasure = False
collisionEnemy = False
movingRight = True
name = ''
# create a python list/array of enemies
enemies = [(enemyX, enemyY, movingRight)]

while finished == False:  # This means while the game is not finished
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            finished = True

    
    pressedKeys =   pygame.key.get_pressed() #[....,UP, DOWN, SPACE,...]

    enemyIndex = 0
    for enemyX, enemyY, movingRight in enemies:
        if enemyX >= 500 -35:
            movingRight = False
        elif enemyX <= 75:
            movingRight = True
        if movingRight == True:
            enemyX += 5 * level
        else:
            enemyX -= 5 * level
        enemies[enemyIndex] = (enemyX, enemyY, movingRight)
        enemyIndex += 1

    if pressedKeys[pygame.K_SPACE] == 1:
        y -= 3 # this means y = y +5
        
    # rectOne = pygame.Rect(x,y,20,20) # we define a x, y, width and height of the rectangle

    color = (0,0,255) # specify the RGB values required - Tuple
    black = (0, 0, 0) # RGB for black
    # screen.fill(blue) - This overwrites our backgroundimage - test backgroud
    screen.blit(backgroundImage,(0,0))
    screen.blit(treasureImage, (treasureX, treasureY))
    screen.blit(playerImage, (x,y)) # so it is overlaid on display

    
    enemyIndex = 0
    for enemyX, enemyY, movingRight in enemies:    # for each of these items
        screen.blit(enemyImage, (enemyX, enemyY))   # load another enemy on to the screen
        collisionEnemy, y = checkCollision(x, y, enemyX, enemyY)
        if collisionEnemy == True:
            name = enemyNames[enemyIndex]
            textLose  = font.render("Captured by" + name, True, (255,185,0))
            screen.blit(textLose,(300 - textLose.get_width()/2, 300 - textLose.get_height(0/2)))
            pygame.display.flip()
            frame.tick(1)
            frame.tick(30)
            enemyIndex += 1
        
    collisionTreasure, y = checkCollision(x, y, treasureX, treasureY)
    #collisionEnemy, y = checkCollision(x, y, enemyX, enemyY)

    if collisionTreasure == True:
        level += 1
        enemies.append((enemyX-30*level, enemyY-30*level, False))
        textWin = font.render("Level Reached!" + str(level), True, (0,0,0))
        screen.blit(textWin, (300 - textWin.get_width()/2, 275 -textWin.get_height()/2))
        pygame.display.flip()
        frame.tick(1)
    #elif collisionEnemy == True:
        #collisionEnemy = False

    # pygame.draw.rect(screen, color, rectOne)
    pygame.display.flip()
    frame.tick(30) # this pauses the mvmt by 1/30th of a sec.


